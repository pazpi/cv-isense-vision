# StructureViewerSwift

Swift based app which shows the depth captured by the [Structure Sensor](http://structure.io/) and with information from the normal frame estimated determine the best route around obstacle

Code and report complete can be found [here](https://gitlab.com/pazpi/cv-isense-vision)
