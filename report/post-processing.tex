\section{Post Processing}

La prima operazione da fare e' di ottenere il frame catturato dal sensore iSense che per facilitare l'utilizzo viene presentato dall'SDK come una matrice di dimensione 640x480 i cui valori rappresentano la distanza in millimetri dall'ottica.

Per visualizzare questo frame in maniera visivamente chiara viene applicata una colorazione in falsi colori dove il colore verde indica un'ostacolo vicino mentre il colore blu un'ostacolo in lontananza come si può vedere in figura \ref{fig:depthframe}.

\begin{figure}[h]
	\centering
	\includegraphics[width=12cm]{img/depth-frame.png}
	\caption{Esempio di composizione in falsi colori della mappa di profondità}
	\label{fig:depthframe}
\end{figure}

Questa mappa della distanza in falsi colori non è particolarmente informativa, è utile a capire come il sensore percepisce gli ambienti e valutarne i limiti, esistono infatti zone completamente bianche che indicano come il sensore non riesca a rilevare nulla, solitamente questo accade in zone dove è presente una forte illuminazione solare.
Purtroppo questa è una forte limitazione che confina l'utilizzo del sensore ai soli ambienti interni.

Nel \textit{Software Development Kit (SDK)} fornito dai creatori stessi del sensore sono presenti molte funzioni specifiche per ottenere informazioni differenti dalla matrice di partenza, una di queste consente di calcolare il vettore normale delle superfici.
Questo sarà il nostro punto di partenza per stimare gli ostacoli e suggerire una traiettoria preferenziale.

La funzione \texttt{calculateNormals(with: depthFrame)} ci permette di ottenere una matrice di vettori delle stesse dimensioni dell'immagine di partenza. Ogni cella della matrice contiene le tre coordinate cartesiane x y e z del vettore normale.

Come per le informazioni sulla profondità, anche questa matrice sarebbe impossibile da visualizzare senza una composizione in falsi colori.
Viene eseguita una trasformazione lineare per portare i valori ottenuti compresi fra -1 e 1 nel range 0 $\div$ 255, ottenendo un'immagine RGB a 8 bit come si può vedere in figura \ref{fig:normalframe}.

\begin{figure}[h]
	\centering
	\includegraphics[width=12cm]{img/normal-RGB.png}
	\caption{Esempio di composizione in falsi colori dei vettori normali alle superfici, xyz $\rightarrow$ RGB}
	\label{fig:normalframe}
\end{figure}

Come si può vedere dall'immagine il pavimento e' riconoscibile dal suo colore bordò molto accesso. Una particolarità di questa immagine è che le gradazioni di colore avvengono cambiando colore ai pixel in un intorno. Ad esempio se si ruota il tablet lungo l'asse orizzontale si nota come la differenza degli angoli non avviene in maniera uniforme su ogni pixel ma invece a macchie sempre più grandi fino a riempire l'intera zona con il colore dato dalla nuova orientazione della superficie come si nota nel dettaglio di figura \ref{fig:normalframe-angle}.

\begin{figure}[h]
	\centering
	\includegraphics[width=12cm]{img/normal-RGB-angle-crop.png}
	\caption{Dettaglio su normal frame con una angolazione di 45\degree ca. rispetto al pavimento. Si possono notare anche le barre verticale date dalla creazione di questa immagine}
	\label{fig:normalframe-angle}
\end{figure}

Un'altra problematica legata alle informazioni delle normali delle superfici è data dalla presenza di barre verticali lungo tutta l'immagine non più grandi di due o tre pixel.
Non si può sapere questo fenomeno a cos'è dovuto in quento tutta l'implementazione di questo è closed source e che la loro posizione e numero sembrerebbe casuale non è possibile applicare dei filtri specifici. 
Questo ha creato dei problemi in seguito perché l'immagine segmentata non sarà uniforme ma conterrà molti puntini sparsi per l'immagine di grandezza variabile, difficili da togliere con un semplice filtro sale-pepe.

\subsection{Segmentazione delle superfici orizzontali}

Analizzando l'immagine in falsi colori dei vettori normali possiamo vedere dall'istogramma di una superficie orizzontale come il canale rosso e quindi la coordinata x e il canale blu, la coordinata z, presentano un solo picco con pochissima varianza mentre la coordinata y e quindi il colore verde è spalmato su una gamma di valori più ampia, come si vede in figura \ref{fig:section-normal-frame}.

\begin{figure}[h]
	\centering
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=.9\linewidth]{img/dettaglio-normalframe.png}
		\caption{Sezione del pavimento di figura \ref{fig:normalframe}}
		\label{fig:section-normal-frame-subcaption}
	\end{subfigure}%
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=.9\linewidth]{img/normalframe-histogram.png}
		\caption{Istogramma dalla sezione delimitata in bianco in figura \ref{fig:section-normal-frame-subcaption}}
		\label{fig:section-normal-frame-histogram}
	\end{subfigure}
	\caption{Dettaglio del frame in falso colore dei vettori normali e del rispettivo istogramma}
	\label{fig:section-normal-frame}
\end{figure}

Per discriminare il pavimento basterebbe filtrare i pochi valori che compongono la coordinata x e z, purtroppo non è efficace perché assieme al pavimento vengono discriminate zone con angolazioni differenti.
Per questo la scelta ottimale ricade nel filtrare usando solo le informazioni della coordinata y.

Questa soluzione risulta anche più comoda da usare dato che l'immagine segmentata sarà di tipo binario riducendo la complessità dei filtri successivi usati per togliere il rumore rimanente.


\subsection{Filtri mediani e morfologici}

Come si può vedere in figura \ref{fig:horrizontal-no-filter} l'immagine binaria segmentata è molto rumorosa anche se rappresenta un ottimo punto di partenza.
Il primo filtro che viene applicato è un filtro mediano che elimina la maggior parte dei pixel bianchi sparsi da considerarsi rumore.

\begin{figure}[h]
	\centering
	\includegraphics[width=12cm]{img/only-horrizontal.png}
	\caption{Estrazione delle superfici orizzontali sfruttando solo la componente y del vettore normale}
	\label{fig:horrizontal-no-filter}
\end{figure}

L'ambiente di sviluppo iOS fornisce molti filtri molto ben ottimizzati che sfruttano la GPU interna, purtroppo alcuni di questi non dispongono di parametri configurabili come il filtro mediano dove la dimensione del kernel applicato è sconosciuta e nemmeno impostabile.
Applicando una volta questo filtro l'immagine risulta comunque molto più pulita ma rimangono ancora degli artefatti che impediscono una elaborazione del percorso ottimale.

Il secondo filtro applicato è di tipo morfologico di erosione che viene applicato per rimuovere la componente di rumore che un filtro mediano non riesce a togliere.
Anche in questo caso l'ambiente di sviluppo mette a disposizione dei metodi per questo filtro ma a differenza di quello mediano possiamo specificare i vari parametri del filtro.
Il kernel scelto per il filtro morfologico è una matrice di dimensioni $11\times11$ composta da soli 1, questo ci consentirà di rimuovere i gruppi di pixel più grandi sulle superfici non orizzontali.

L'applicazione di questo filtro risulta molto onerosa in termini di cicli di calcolo del processore comportando una significativa crescita nell'uso della CPU. A causa delle politiche di iOS verso le applicazioni questo comporta una diminuzione del numero del frame rate al quale riusciamo a calcolare il percorso, difatti il sistema preferisce concedere meno risorse piuttosto che venire rallentato nel suo insieme.
Proprio per questo motivo l'applicazione di un secondo filtro morfologico risulta impossibile, dato che si processerebbe un frame ogni pochi secondi, difatti la combinazione di un filtro \textit{erode} ed di un filtro \textit{dilate} avrebbe consentito una pulizia maggiore del frame finale.
Questo porta ad un'immagine finita non ottimale per riuscire a distinguere in maniera automatica il pavimento per questo verranno implementati dei metodi di ricerca che terranno conto anche di un intorno di pixel rispetto a quello in esame.
Per ovviare a questo inconveniente vengono applicati due volte in cascata il filtro mediano che ci consente di rifinire un poco l'immagine sopratutto nelle zone orizzontali tenendo un frame rate comunque accettabile.

Il risultati di questi filtri si possono vedere nella figura \ref{fig:horr-finito-filtraggio}, forse l'immagine finale potrebbe essere pulita ulteriormente aggiungendo altri filtri mediani in cascata.

\begin{figure}[h]
	\centering
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=.9\linewidth]{img/after-erode.png}
		\caption{Immagine risultante con solo filtro erosione}
		%\label{fig:horr-solo-erode}
	\end{subfigure}%
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=.9\linewidth]{img/after-all-filter.png}
		\caption{Immagine finale dopo anche i filtri mediani}
		\label{fig:horr-anche-dopo-mediano}
	\end{subfigure}
	\caption{}
	\label{fig:horr-finito-filtraggio}
\end{figure}