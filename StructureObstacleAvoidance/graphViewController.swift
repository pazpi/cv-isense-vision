//
//  graphViewViewController.swift
//  StructureObstacleAvoidance
//
//  Created by mmlab on 23/05/2018.
//  Copyright © 2018 Pasetto Davide. All rights reserved.
//

import UIKit


class graphViewViewController: UIViewController {

    @IBOutlet weak var stepper_green_max: UIStepper!
    @IBOutlet weak var stepper_green_min: UIStepper!
    @IBOutlet weak var stepper_erode_filter: UIStepper!
    @IBOutlet weak var stepper_false_positive_radius: UIStepper!
    @IBOutlet weak var stepper_false_positive_ratio: UIStepper!
    
    @IBOutlet weak var text_green_max: UITextField!
    @IBOutlet weak var text_green_min: UITextField!
    @IBOutlet weak var text_erode_size: UITextField!
    @IBOutlet weak var text_false_positive_radius: UITextField!
    @IBOutlet weak var text_false_positive_ratio: UITextField!
    
    
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Load value from disk
        let green_max = defaults.integer(forKey: "green_max")
        let green_min = defaults.integer(forKey: "green_min")
        let erode_size = defaults.integer(forKey: "erode_filter_size")
        let false_positive_radius = defaults.integer(forKey: "false_positive_radius")
        let false_positive_ratio = defaults.integer(forKey: "false_positive_ratio")
        
        stepper_green_max.value = Double(green_max)
        stepper_green_min.value = Double(green_min)
        stepper_erode_filter.value = Double(erode_size)
        stepper_false_positive_radius.value = Double(false_positive_radius)
        stepper_false_positive_ratio.value = Double(false_positive_ratio)
        
        text_green_max.text = String(Int(green_max))
        text_green_min.text = String(Int(green_min))
        text_erode_size.text = String(Int(erode_size))
        text_false_positive_radius.text = String(Int(false_positive_radius))
        text_false_positive_ratio.text = String(Int(false_positive_ratio))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func save_parameters(_ sender: UIButton) {
        defaults.set(stepper_green_max.value, forKey: "green_max")
        defaults.set(stepper_green_min.value, forKey: "green_min")
        defaults.set(stepper_erode_filter.value, forKey: "erode_filter_size")
        defaults.set(stepper_false_positive_radius.value, forKey: "false_positive_radius")
        defaults.set(stepper_false_positive_ratio.value, forKey: "false_positive_ratio")
    }

    @IBAction func change_green_max(_ sender: UIStepper) {
        text_green_max.text = String(Int(sender.value))
    }
    
    @IBAction func change_green_min(_ sender: UIStepper) {
        text_green_min.text = String(Int(sender.value))
    }

    @IBAction func change_erode_size(_ sender: UIStepper) {
        text_erode_size.text = String(Int(sender.value))
    }
    
    @IBAction func change_false_positive_radius(_ sender: UIStepper) {
        text_false_positive_radius.text = String(Int(sender.value))
    }
    
    @IBAction func change_false_positive_ratio(_ sender: UIStepper) {
        text_false_positive_ratio.text = String(Int(sender.value))
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
