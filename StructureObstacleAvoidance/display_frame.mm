//
//  display_frame.m
//  StructureObstacleAvoidance
//
//  Created by mmlab on 26/04/2018.
//  Copyright © 2018 Pasetto Davide. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Structure/StructureSLAM.h>
#import <Structure/Structure.h>


@interface HandleNormal : NSObject {
    uint16_t *_linearizeBuffer;
    uint8_t *_normalsBuffer;
    UIImageView *_normalsImageView;
    STNormalEstimator *_normalsEstimator;
}

- (void)renderNormalsFrame:(STDepthFrame *)depthFrame;

@end

@implementation HandleNormal

- (void)populateLinearizeBuffer
{
    const uint16_t maxShiftValue = 2048;
    _linearizeBuffer = (uint16_t*)malloc((maxShiftValue + 1) * sizeof(uint16_t));
    
    for (int i=0; i <= maxShiftValue; i++)
    {
        float v = i / (float)maxShiftValue;
        v = powf(v, 3)* 6;
        _linearizeBuffer[i] = v*6*256;
    }
}

- (void) renderNormalsFrame: (STDepthFrame *)depthFrame
{

    NSLog(@"d_cols %d, d_rows %d", depthFrame.width, depthFrame.height);
    NSLog(@"d_depth %f", depthFrame.depthInMillimeters[100]);
    // Estimate surface normal direction from depth float values
    STNormalFrame *normalsFrame = [_normalsEstimator calculateNormalsWithDepthFrame:depthFrame];
    size_t cols = normalsFrame.width;
    size_t rows = normalsFrame.height;
    
    NSLog(@"cols %zu, rows %zu", cols, rows);
    
    // Convert normal unit vectors (ranging from -1 to 1) to RGB (ranging from 0 to 255)
    // Z can be slightly positive in some cases too!
    if (_normalsBuffer == NULL)
    {
        _normalsBuffer = (uint8_t*)malloc(cols * rows * 4);
    }
    for (size_t i = 0; i < cols * rows; i++)
    {
        _normalsBuffer[4*i+0] = (uint8_t)( ( ( normalsFrame.normals[i].x / 2 ) + 0.5 ) * 255);
        _normalsBuffer[4*i+1] = (uint8_t)( ( ( normalsFrame.normals[i].y / 2 ) + 0.5 ) * 255);
        _normalsBuffer[4*i+2] = (uint8_t)( ( ( normalsFrame.normals[i].z / 2 ) + 0.5 ) * 255);
    }
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGBitmapInfo bitmapInfo;
    bitmapInfo = (CGBitmapInfo)kCGImageAlphaNoneSkipFirst;
    bitmapInfo |= kCGBitmapByteOrder32Little;
    
    NSData *data = [NSData dataWithBytes:_normalsBuffer length:cols * rows * 4];
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((CFDataRef)data);
    
    CGImageRef imageRef = CGImageCreate(cols,
                                        rows,
                                        8,
                                        8 * 4,
                                        cols * 4,
                                        colorSpace,
                                        bitmapInfo,
                                        provider,
                                        NULL,
                                        false,
                                        kCGRenderingIntentDefault);
    
    _normalsImageView.image = [[UIImage alloc] initWithCGImage:imageRef];
    
    CGImageRelease(imageRef);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorSpace);
}


@end
