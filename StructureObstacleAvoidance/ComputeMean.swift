//
//  ComputeMean.swift
//  StructureObstacleAvoidance
//
//  Created by mmlab on 24/06/2018.
//  Copyright © 2018 Pasetto Davide. All rights reserved.
//

import Foundation

class ComputeMean {
    
    
    // Obstacle Threashold
    let OBSTHRS : Float = 2
    
    
    // Compute the mean of distance in the point of the square
    func computeMean(_ distArray : UnsafeMutablePointer<Float>, shift: Int, width: Int, height: Int) -> Float{
        var som:Float = 0, pos:Int, k:Int = 0, mean:Float
        for var i in -50...50{
            for var j in -50...50{
                pos = Int(width) * Int(height/2 + j) + Int(width/2 + i + shift)
                if(!distArray[pos].isNaN){
                    som += distArray[pos]
                    k += 1
                }
                j += 1
            }
            i += 1
        }
        mean = som/(Float(1000*k))
        
        if(mean.isNaN)
        {
            // If the mean is NOT A NUMBER return maximum distance (10 m)
            return 10.0
        }
        else
        {
            // print(mean)
            return mean
        }
    }
    
    
    // Check if there are square of point more far than 3 m on the left
    func checkSX(_ distArray : UnsafeMutablePointer<Float>, width: Int, height: Int) -> (goOn: Bool, shift: Int, mean: Float){
        var shift: Int = -1
        while ((computeMean(distArray, shift : shift, width :width, height : height) < OBSTHRS) && (shift > -260)){
            shift -= 5
        }
        if(shift == -260){
            // There is no more space SX
            return (false, shift, computeMean(distArray, shift : shift, width :width, height : height))
        }
        else{
            // There is free space SX
            return (true, shift, computeMean(distArray, shift : shift, width :width, height : height))
        }
    }
    
    
    // Check if there are square of point more far than 3 m on the right
    func checkDX(_ distArray : UnsafeMutablePointer<Float>, width: Int, height: Int) -> (goOn: Bool, shift: Int, mean: Float){
        var shift: Int = 1
        while ((computeMean(distArray, shift : shift, width :width, height : height) < OBSTHRS) && (shift < 260)){
            shift += 5
        }
        if (shift == 260){
            return (false, shift, computeMean(distArray, shift : shift, width :width, height : height))
        }
        else {
            return (true, shift, computeMean(distArray, shift : shift, width :width, height : height))
        }
    }
    
    
    // Inverse check: how far is the obstacle on the right
    func invCheckDX(_ distArray : UnsafeMutablePointer<Float>, width: Int, height: Int, shift: Int) -> (goOn: Bool, shift: Int, mean: Float){
        var shift = shift
        
        while ((computeMean(distArray, shift : shift, width :width, height : height) > OBSTHRS) && (shift < 260)){
            shift += 1
        }
        if(shift == 260){
            return (false, shift, computeMean(distArray, shift : shift, width :width, height : height))
        }
        else{
            return (true, shift, computeMean(distArray, shift : shift, width :width, height : height))
        }
    }
    
    
    // Inverse check: how far is the obstacle on the left
    func invCheckSX(_ distArray : UnsafeMutablePointer<Float>, width: Int, height: Int, shift: Int) -> (goOn: Bool, shift: Int, mean: Float){
        var shift = shift
        
        while ((computeMean(distArray, shift : shift, width :width, height : height) > OBSTHRS) && (shift > -260)){
            shift -= 1
        }
        if(shift == -260){
            // There is no more space SX
            return (false, shift, computeMean(distArray, shift : shift, width :width, height : height))
        }
        else{
            // There is free space SX
            return (true, shift, computeMean(distArray, shift : shift, width :width, height : height))
        }
    }
    
}
