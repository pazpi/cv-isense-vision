//
//  ViewController.swift
//  StructureObstacleAvoidance
//
//  Created by Ugo Alberto Simioni on 18/02/16.
//  Modified end improved by Davide Pasetto from 04/18 to 08/18
//

import UIKit
import GLKit
import Accelerate
import AVFoundation

import os.log
import UIKit


class ViewController: UIViewController, STSensorControllerDelegate {

    @IBOutlet weak var switch_color_int_float: UISwitch!
    @IBOutlet weak var select_view: UISegmentedControl!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var depthView: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var IndicationLabel: UIBarButtonItem!
    @IBOutlet weak var stepper_segment: UIStepper!
    
    let my_log = OSLog(subsystem: "com.strucutre.main", category: "mine")
    let my_log_audio = OSLog(subsystem: "com.strucutre.main.audio", category: "audio")
    
    let monitor_resource = monitorResource()
    
    // Audio Player
    var audioPlayer: AVAudioPlayer!
    
    // Helper class to convert float depth data to RGB values for better visualization.
    var toRGBA : STDepthToRgba?
    var normalFrame = STNormalEstimator()
    
    // var normal_RGB = UIImage()
    var horrizontal_RGB = UIImage()
    var horrizontal_gray = UIImage()
    var depth_RGB = UIImage()
    
    let FRAME_WIDTH = 640
    let FRAME_HEIGHT = 480
    
    let saved_parameter = UserDefaults.standard
    
    let post_process = postProcessing()
    let inspect_route = inspectRoute()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Inizialize the sensor
        STSensorController.shared().delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.appDidBecomeActive), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    
    @objc func appDidBecomeActive() {
        // Start Streaming of the Depth view
        if STSensorController.shared().isConnected() {
            _ = tryStartStreaming()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Start Streaming of the Depth view
        if tryInitializeSensor() && STSensorController.shared().isConnected() {
            _ = tryStartStreaming()
        } else {
            print("Status: Disconnected")
            statusLabel.text = "Disconnected"
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        switch_color_int_float.setOn(false, animated: false)
        select_view.selectedSegmentIndex = 1
        stepper_segment.value = 4.0
        infoLabel.isHidden = true
    }

    func tryInitializeSensor() -> Bool {
        let result = STSensorController.shared().initializeSensorConnection()
        if result == .alreadyInitialized || result == .success {
            return true
        }
        return false
    }
    
    func tryStartStreaming() -> Bool {
        if tryInitializeSensor() {
            // Inizialize the sensor that actice the TOF camera with the maximum resolution (640x480)
            let options : [AnyHashable: Any] = [
                kSTStreamConfigKey: NSNumber(value: STStreamConfig.depth640x480.rawValue),
                kSTFrameSyncConfigKey: NSNumber(value: STFrameSyncConfig.off.rawValue),
                // kSTHoleFilterConfigKey: true
                kSTHoleFilterEnabledKey: true
            ]
            do {
                try STSensorController.shared().startStreaming(options: options as [AnyHashable: Any])
                // Option of the RGBA immage (RedToBlueGradient -> map of distance Red = closest point, Blue = farthest point, otherwise we could choose a linear Gray scale
                let toRGBAOptions : [AnyHashable: Any] = [
                    kSTDepthToRgbaStrategyKey : NSNumber(value: STDepthToRgbaStrategy.redToBlueGradient.rawValue)
                ]
                toRGBA = STDepthToRgba(options: toRGBAOptions)
                return true
            } catch let error as NSError {
                print(error)
            }
        }
        return false
    }
    
    func sensorDidConnect() {
        if tryStartStreaming() {
            statusLabel.text = "Streaming"

        } else {
            statusLabel.text = "Connected"
        }
    }
    
    func sensorDidDisconnect() {
        print("Status: Disconnected")
        statusLabel.text = "Disconnected"
    }
    
    func sensorDidStopStreaming(_ reason: STSensorControllerDidStopStreamingReason) {
        statusLabel.text = "Stopped Streaming"
    }
    
    func sensorDidLeaveLowPowerMode() { }
    
    func sensorBatteryNeedsCharging() {
        statusLabel.text = "Low Battery"
    }
    
    
    // Main funciton, process depth frame into normal and segment pavement. Also compute best path
    func sensorDidOutputDepthFrame(_ depthFrame: STDepthFrame!) {
        var best_path_string: String = ""
        // let pixel_90 = inspect_route.pixelUnserLine(Double(90))
        
        switch select_view.selectedSegmentIndex {
        // Frame with False Color Composition, XYZ -> RGB
        case 0:
            let normal_frame = normalFrame.calculateNormals(with: depthFrame)
            var normal_RGB = post_process.normal_false_color(normal_frame!).uiimage!
            normal_RGB = post_process.median_filter(normal_RGB)!
            depthView!.image = normal_RGB
        
        // Frame with only y component
        case 1:
            
            let normal_frame = normalFrame.calculateNormals(with: depthFrame)
            
            // Use the orginal float value to identify the horrizontal plane
            let horrizontal_rgb = post_process.horrizontal_plane_float_normal_RGBA(normal_frame!)
            var horrizontal_rgb_ci = CIImage(image: horrizontal_rgb.uiimage!)
            horrizontal_rgb_ci = horrizontal_rgb_ci?.applyingFilter("CIMedianFilter")
            horrizontal_rgb_ci = horrizontal_rgb_ci?.applyingFilter("CIMedianFilter")
            
            let context = CIContext(options: nil)
            var horrizontal_rgb_cg = context.createCGImage(horrizontal_rgb_ci!, from: (horrizontal_rgb_ci!.extent))
            
            
            let kernel_size = saved_parameter.integer(forKey: "erode_filter_size")
            let kernel: Array<Float> = Array(repeating: 1.0, count: kernel_size*kernel_size)
            
            horrizontal_rgb_cg = post_process.erode_filter(horrizontal_rgb_cg!, kernel: kernel)
            
            
            // debug
            horrizontal_rgb_ci = CIImage(cgImage: horrizontal_rgb_cg!)
            
            horrizontal_rgb_ci = horrizontal_rgb_ci?.applyingFilter("CIMedianFilter")
            horrizontal_rgb_ci = horrizontal_rgb_ci?.applyingFilter("CIMedianFilter")
            
            let filteredCGImage = context.createCGImage(horrizontal_rgb_ci!, from: horrizontal_rgb_ci!.extent)
            
            let filteredUIImage = UIImage(cgImage: filteredCGImage!)
            
            let score = inspect_route.computeScore(filteredUIImage, num_of_segment: Int(stepper_segment.value), depth_frame: depthFrame)
            let best_path = inspect_route.bestAngle(score)
            best_path_string = "Best Angle: \(best_path.angle) with score \(best_path.score)"
            
            let horrizontal_with_line = drawMultipleLine(Int(stepper_segment.value))
            
            var composite_image = compositeImage(image1: filteredCGImage!,
                                                 image2: horrizontal_with_line.cgimage!,
                                                 blend_mode: CGBlendMode.difference)
            composite_image = compositeImage(image1: composite_image!,
                                             image2: colorLineWithAngle(best_path.angle, color: UIColor.green).cgimage!,
                                             blend_mode: CGBlendMode.difference)
            
            if switch_color_int_float.isOn {
                let depth_RGB_pixel = toRGBA?.convertDepthFrame(toRgba: depthFrame)
                let depth_cgimage = post_process.imageFromPixels(depth_RGB_pixel!, width: FRAME_WIDTH, height: FRAME_HEIGHT).cgimage
                composite_image = compositeImage(image1: depth_cgimage!,
                                                 image2: composite_image!,
                                                 blend_mode: CGBlendMode.multiply)
            }
            
            let composite_ui = UIImage(cgImage: composite_image!)
            depthView!.image = composite_ui

        case 2:
            // Classic depth view
            if let renderer = toRGBA {
                // convert the Depth Frame To Rgba for a better visualization
                let pixels = renderer.convertDepthFrame(toRgba: depthFrame)
                // Add the new depth frame in RGBA
                depthView!.image = post_process.imageFromPixels(pixels!, width: Int(renderer.width), height: Int(renderer.height)).uiimage
            }
        default:
            
            if let renderer = toRGBA {
                // convert the Depth Frame To Rgba for a better visualization
                let pixels = renderer.convertDepthFrame(toRgba: depthFrame)
                // Add the new depth frame in RGBA
                depthView!.image = post_process.imageFromPixels(pixels!, width: Int(renderer.width), height: Int(renderer.height)).uiimage
            }
        }

        let memory_usage = monitor_resource.memoryUseage()
        let memory_usage_mb = UInt32(memory_usage.usedMemory/(1000*1000))
        let memory_total_mb = UInt32(memory_usage.totalMemory/(1000*1000))
        statusLabel.textColor = UIColor.white
        statusLabel.text = "\(best_path_string) - USAGE CPU: \(Float(monitor_resource.cpuUsage()))% Memory: \(memory_usage_mb)/\(memory_total_mb) MB - \(Float(memory_usage_mb/memory_total_mb))"
        
         // How to play audio file
         // if(IndicationLabel.title != "STRAIGHT"){
         // IndicationLabel.title = "STRAIGHT"
         // audioPlayer = setupAudioPlayerWithFile("straight", type: "aiff")
         // audioPlayer.play()
         // }
    }
    
    
    // Get list of pixel composing a line
    func arrayOfPixelUnderLine(_ num_of_segment: Int) -> Array<Array<inspectRoute.PixelPair>> {
        let angles_list = inspect_route.anglesVector(num_of_segment)
        var pixels_line: Array<Array<inspectRoute.PixelPair>> = []
        
        for angle in angles_list {
            pixels_line.append(inspect_route.pixelUnderLine(Double(angle)))
        }
        return pixels_line
    }
    
    
    //
    func colorLineWithAngle(_ angle: Double, color: UIColor)  -> (uiimage: UIImage?, cgimage: CGImage?) {
        var new_angle: Double = angle
        if angle > 90.0 {
            new_angle -= (angle-90.0)*2
        } else {
            new_angle += (90.0-angle)*2
        }
        let pixels_line = inspect_route.pixelUnderLine(Double(new_angle))
        return drawLine(pixels_line, color: color)
    }
    
    
    // Draw line for path inspecting
    func drawMultipleLine(_ num_of_segment: Int) -> (uiimage: UIImage?, cgimage: CGImage?) {
        let angles_list = inspect_route.anglesVector(num_of_segment)
        let color = UIColor.red
        var pixels_line: Array<inspectRoute.PixelPair> = []
        
        for angle in angles_list {
            pixels_line += inspect_route.pixelUnderLine(Double(angle))
        }
        
        return drawLine(pixels_line, color: color)
    }
    
    
    // Draw line for path inspecting
    func drawLine(_ pixels_line: Array<inspectRoute.PixelPair>, color: UIColor) -> (uiimage: UIImage?, cgimage: CGImage?) {
        let frame_pixel_number: Int = FRAME_WIDTH * FRAME_HEIGHT
        var red: CGFloat = 0.0
        var green: CGFloat = 0.0
        var blue: CGFloat = 0.0
        var alpha: CGFloat = 0.0
        color.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        
        // empy frame buffer
        let line_frame_buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: frame_pixel_number*4)
        line_frame_buffer.initialize(to: 0)
        for pair in pixels_line {
            let real_pixel = inspectRoute.PixelPair(x: (FRAME_WIDTH/2 + pair.x), y: (FRAME_HEIGHT - pair.y))
            let index = (real_pixel.y * FRAME_WIDTH-1) - real_pixel.x
            
            line_frame_buffer[4*index+0] = UInt8(red) * 255
            line_frame_buffer[4*index+1] = UInt8(green) * 255
            line_frame_buffer[4*index+2] = UInt8(blue) * 255
            line_frame_buffer[4*index+3] = UInt8(alpha) * 255
        }
        
        let images = post_process.imageFromPixels(line_frame_buffer, width: FRAME_WIDTH, height: FRAME_HEIGHT)
        
        line_frame_buffer.deallocate()
        return images
    }
    
    
    // Merge two CGImage with method CGBlendMode
    func compositeImage(image1: CGImage, image2: CGImage, blend_mode: CGBlendMode) -> CGImage? {
        let bounds1 = CGRect(x: 0, y: 0, width: image1.width, height: image1.height)
        let bounds2 = CGRect(x: 0, y: 0, width: image2.width, height: image2.height)
        // let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedFirst.rawValue)
        let ctx = CGContext(data: nil,
                            width: image1.width,
                            height: image1.height,
                            bitsPerComponent: image1.bitsPerComponent,
                            bytesPerRow: image1.bytesPerRow,
                            space: image1.colorSpace!,
                            bitmapInfo: bitmapInfo.rawValue)!
        ctx.draw(image1, in: bounds1)
        // ctx.setBlendMode(CGBlendMode.difference)
        ctx.setBlendMode(blend_mode) // one image over the other
        ctx.draw(image2, in: bounds2)
        return ctx.makeImage()!
    }
    
    
    // Draw rect function
    func rectImage(_ parentView: UIView) -> UIView {
        let rettan: UIView = UIView(frame: parentView.bounds)
        rettan.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        rettan.bounds = CGRect(x: 0, y: 0, width: FRAME_WIDTH, height: FRAME_HEIGHT)
        
        let subrect = UIView(frame: CGRect(
            x: rettan.frame.width * 0.5 - 50,
            y: rettan.frame.height * 0.5 - 50,
            width: 100,
            height: 100))
        subrect.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        subrect.layer.borderColor = UIColor.red.cgColor
        subrect.layer.borderWidth = 1.0
        rettan.addSubview(subrect)
        
        let subrect2 = UIView(frame: CGRect(
            x: rettan.frame.width * 0.5 - 50,
            y: rettan.frame.height * 0.9 - 50,
            width: 100,
            height: 100))
        subrect2.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        subrect2.layer.borderColor = UIColor.red.cgColor
        subrect2.layer.borderWidth = 1.0
        rettan.addSubview(subrect2)
        return rettan
    }
 
    
    // Set up the audio file
    func setupAudioPlayerWithFile(_ file:NSString, type:NSString) -> AVAudioPlayer  {
        let path = Bundle.main.path(forResource: file as String, ofType: type as String)
        let url = URL(fileURLWithPath: path!)
        var audioPlayer:AVAudioPlayer?
        os_log("Playing file: %@", log: my_log_audio, type: .debug, file)
        do {
            try audioPlayer = AVAudioPlayer(contentsOf: url)
        } catch {
            os_log("NO AUDIO FILE", log: my_log_audio, type: .debug)
        }
        
        return audioPlayer!
    }
}

