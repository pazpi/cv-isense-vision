//
//  InspectRoute.swift
//  StructureObstacleAvoidance
//
//  Created by mmlab on 03/07/2018.
//  Copyright © 2018 Pasetto Davide. All rights reserved.
//

import Foundation
import os.log


extension UIImage {
    
    subscript (x: Int, y: Int) -> UIColor? {
        
        if x < 0 || x > Int(size.width) || y < 0 || y > (Int(size.height)-1) {
            return nil
        }
        
        //let provider = CGImageGetDataProvider(self.cgImage!)
        let provider = self.cgImage!.dataProvider
        // let providerData = CGDataProviderCopyData(provider!)
        let providerData = provider?.data
        let data = CFDataGetBytePtr(providerData)
        
        let numberOfComponents = 4
        let pixelData = ((Int(size.width) * y) + x) * numberOfComponents
        
        let r = CGFloat(data![pixelData]) / 255.0
        let g = CGFloat(data![pixelData + 1]) / 255.0
        let b = CGFloat(data![pixelData + 2]) / 255.0
        let a = CGFloat(data![pixelData + 3]) / 255.0
        
        return UIColor(red: r, green: g, blue: b, alpha: a)
    }
}


extension UIColor {
    
    func rgb() -> Int? {
        var fRed : CGFloat = 0
        var fGreen : CGFloat = 0
        var fBlue : CGFloat = 0
        var fAlpha: CGFloat = 0
        if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
            let iRed = Int(round(fRed) * 255.0)
            let iGreen = Int(round(fGreen) * 255.0)
            let iBlue = Int(round(fBlue) * 255.0)
            let iAlpha = Int(round(fAlpha) * 255.0)
            
            //  (Bits 24-31 are alpha, 16-23 are red, 8-15 are green, 0-7 are blue).
            let rgb = (iAlpha << 24) + (iRed << 16) + (iGreen << 8) + iBlue
            return rgb
        } else {
            // Could not extract RGBA components:
            return nil
        }
    }
}


extension Array where Element: Hashable {
    func removingDuplicates() -> [Element] {
        var addedDict = [Element: Bool]()
        
        return filter {
            addedDict.updateValue(true, forKey: $0) == nil
        }
    }
    
    mutating func removeDuplicates() {
        self = self.removingDuplicates()
    }
}


class inspectRoute {
    
    let my_log = OSLog(subsystem: "com.strucutre.inspectRoute", category: "mine")
    
    let x_max = 640
    let y_max = 479
    
    let saved_parameter = UserDefaults.standard
        
    struct PixelPair: Hashable {
        var x: Int = 0
        var y: Int = 0
    }
    
    struct PathScore {
        var angle: Double = 0
        var score: Int = 0
    }
    
    
    // Use the number of pure white pixel along that line, when cross a black pixel check sourranding. otherwise multiply by max distance to get the final score
    func computeScore(_ image: UIImage, num_of_segment: Int, depth_frame: STDepthFrame) -> Array<PathScore> {
        let max_radius = saved_parameter.integer(forKey: "false_positive_radius")
        var score = [PathScore]()
        let angles_list = anglesVector(num_of_segment).sorted()
        
        angle_loop: for angle in angles_list {
            var temp_score: Int = 0
            let pixel_line = realPixelIndex(pixelUnderLine(Double(angle)))
            pixel_loop: for pixel in pixel_line {
                let color = image[pixel.x, pixel.y]
                
                if ( color?.rgb() == 0xffffffff ) {
                    temp_score += 1
                }
                else {
                    // check for surrounding pixel
                    let surr_pixel = surroundingPixel(pixel, image: image, max_radius: max_radius)
                    if falsePositivePixel(surr_pixel, image: image) {
                        temp_score += 1
                    } else {
                        let depth = distanceNearPixel(depth_frame, pixel_pair: pixel)
                        if (depth == Float.nan) {
                            break
                        } else {
                            temp_score *= Int(depth!)
                            break
                        }
                        
                    }
                }
            }
            
            score.append(PathScore.init(angle: angle, score: temp_score))
        }
        score = regolarizeParameter(score, sigma: 10.0)
        return score
    }
    
    
    // Return the real index of pixels
    func realPixelIndex(_ pixels_pair: Array<PixelPair>) -> Array<PixelPair> {
        var real_pixel = [PixelPair]()
        for pixel in pixels_pair {
            real_pixel.append(PixelPair(x: (x_max/2 + pixel.x), y: (y_max - pixel.y)))
        }
        return real_pixel
    }
    
    
    // List of angles in which to divide the frame
    func anglesVector(_ num_segment: Int) -> Array<Double> {
        let split_step: Double = 90.0 / Double(num_segment + 1)
        var angle_list = [Double]()
        for i in 1...(90 / Int(split_step)) {
            let value: Double = split_step * Double(i)
            if Int(value) == 90 {
                angle_list.append(90.0)
            } else {
                angle_list.append(split_step * Double(i))
                angle_list.append((split_step * Double(i))+90)
            }
        }
        return angle_list
    }
    
    
    // Extract the best score, just look for best score into array
    func bestAngle(_ angle_list: Array<PathScore>) -> PathScore {
        var max_value: Int = 0
        var path_score = PathScore()
        for pathscore in angle_list {
            if (pathscore.angle != 0) || (pathscore.angle != 180) {
                if pathscore.score > max_value {
                    max_value = pathscore.score
                    path_score = pathscore
                }
            }
        }
        return path_score
    }
    
    
    // https://en.wikipedia.org/wiki/Midpoint_circle_algorithm
    func surroundingPixel(_ pixel:PixelPair, image: UIImage, max_radius: Int) -> Array<PixelPair> {
        var pixels = [PixelPair]()
        
        for radius in 1...max_radius {
            
            var x = radius-1
            var y = 0
            var dx = 1
            var dy = 1
            var err = dx - (radius << 1)
            
            let x0 = pixel.x
            let y0 = pixel.y
            
            while (x >= y) {
                
                if image[x0 + x, y0 + y] != nil {
                    pixels.append(PixelPair(x: x0+x, y: y0+y))
                }
                
                if image[x0 + y, y0 + x] != nil {
                    pixels.append(PixelPair(x: x0+y, y: y0+x))
                }
                
                if image[x0 - y, y0 + x] != nil {
                    pixels.append(PixelPair(x: x0-y, y: y0+x))
                }
                
                if image[x0 - x, y0 + y] != nil {
                    pixels.append(PixelPair(x: x0-x, y: y0+y))
                }
                
                if image[x0 - x, y0 - y] != nil {
                    pixels.append(PixelPair(x: x0-x, y: y0-y))
                }
                
                if image[x0 - y, y0 - x] != nil {
                    pixels.append(PixelPair(x: x0-y, y: y0-x))
                }
                
                if image[x0 + y, y0 - x] != nil {
                    pixels.append(PixelPair(x: x0+y, y: y0-x))
                }
                
                if image[x0 + x, y0 - y] != nil {
                    pixels.append(PixelPair(x: x0+x, y: y0-y))
                }
                
                if (err <= 0)
                {
                    y += 1
                    err += dy
                    dy += 2;
                }
                
                if (err > 0)
                {
                    x -= 1 ;
                    dx += 2;
                    err += dx - (radius << 1);
                }
            }
        }
        return pixels
    }
    
    
    // Check for false positive pixel, if sourranding pixel is white is treaten as pavement
    func falsePositivePixel(_ pixels:Array<PixelPair>, image: UIImage) -> Bool {
        let false_positive_threshold: Double = Double(saved_parameter.integer(forKey: "false_positive_ratio")) / 100.0
        var white_pixel = 0
        let unique_pixels = pixels.removingDuplicates()
        for pixel in unique_pixels {
            if image[pixel.x, pixel.y]?.rgb() == 0xffffffff {
                white_pixel += 1
            }
        }
        if (Double(white_pixel) / Double(unique_pixels.count)) > false_positive_threshold  {
            return true
        } else {
            return false
        }
    }
    
    
    // Get all pixel under a line given the angle, value ar not relevant to frame
    func pixelUnderLine(_ alpha:Double ) -> Array<PixelPair> {
        let sin_alpha: Double = sin(alpha * Double.pi / 180)
        let cos_alpha: Double = cos(alpha * Double.pi / 180)
        // let r_max: Double = Double(x_max/2 ) * cos_alpha + Double( y_max/2 ) * sin_alpha
        let eps: Double = 1E-3
        let step_min: Double = 0.5 + eps
        var pixels = [PixelPair]()
        var prev_pixel = PixelPair.init(x: -1, y: -1)
        var num_step:UInt = 0
        
        while true {
            var pixel = PixelPair()
            let r: Double = step_min * Double(num_step)
            pixel.x = Int(round(r * cos_alpha))
            pixel.y = Int(round(r * sin_alpha))
            
            if (abs(pixel.x) >= (x_max/2) || abs(pixel.y) >= y_max) {
                return pixels
            }
            
            if !(prev_pixel.x == pixel.x && prev_pixel.y == pixel.y) {
                pixels.append(pixel)
            }
            prev_pixel = pixel
            num_step += 1
        }
    }
    
    
    // Get distance from pixel, sometimes is null so a sourranding check is necessary
    func distanceNearPixel(_ depth_frame: STDepthFrame, pixel_pair: PixelPair) -> Float? {
        if (depth_frame.depthInMillimeters[pixel_pair.x * pixel_pair.y]).isNaN {
            if (depth_frame.depthInMillimeters[pixel_pair.x * (pixel_pair.y+1)]).isNaN {
                if (depth_frame.depthInMillimeters[pixel_pair.x * (pixel_pair.y-1)]).isNaN {
                    if (depth_frame.depthInMillimeters[(pixel_pair.x+1) * pixel_pair.y]).isNaN {
                        if (depth_frame.depthInMillimeters[(pixel_pair.x-1) * pixel_pair.y]).isNaN {
                            return nil
                        } else {
                            return depth_frame.depthInMillimeters[(pixel_pair.x-1) * pixel_pair.y]
                        }
                    } else {
                        return depth_frame.depthInMillimeters[(pixel_pair.x+1) * pixel_pair.y]
                    }
                } else {
                    return depth_frame.depthInMillimeters[pixel_pair.x * (pixel_pair.y-1)]
                }
            } else {
                return depth_frame.depthInMillimeters[pixel_pair.x * (pixel_pair.y+1)]
            }
        } else {
            return depth_frame.depthInMillimeters[pixel_pair.x * pixel_pair.y]
        }
    }

    
    // Correct the score, give preference to 90 degree or go straight
    func regolarizeParameter(_ scores: Array<PathScore>, sigma: Double) -> Array<PathScore> {
        var new_score = [PathScore]()
        for score in scores {
            let score_corrected = sin(score.angle * Double.pi / 180) * Double(score.score)
            let new = PathScore(angle: score.angle, score: Int(score_corrected)/1000)
            new_score.append(new)
        }
        return new_score
    }
    
    // end class
}





