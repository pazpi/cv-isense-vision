//
//  post_processing.swift
//  StructureObstacleAvoidance
//
//  Created by mmlab on 23/06/2018.
//  Copyright © 2018 Pasetto Davide. All rights reserved.
//

import Foundation
import Accelerate


class postProcessing {
    
    
    // Convert raw depth frame in a renderer image
    func imageFromPixels(_ pixels : UnsafeMutablePointer<UInt8>, width: Int, height: Int) -> (uiimage: UIImage?, cgimage: CGImage?) {
        let colorSpace = CGColorSpaceCreateDeviceRGB();
        let bitmapInfo = CGBitmapInfo.byteOrder32Big.union(CGBitmapInfo(rawValue: CGImageAlphaInfo.noneSkipLast.rawValue))
        
        let provider = CGDataProvider(data: Data(bytes: UnsafePointer<UInt8>(pixels), count: width*height*4) as CFData)
        
        let image = CGImage(
            width: width,               //width
            height: height,             //height
            bitsPerComponent: 8,        //bits per component
            bitsPerPixel: 8 * 4,        //bits per pixel
            bytesPerRow: width * 4,     //bytes per row
            space: colorSpace,          //Quartz color space
            bitmapInfo: bitmapInfo,     //Bitmap info (alpha channel?, order, etc)
            provider: provider!,        //Source of data for bitmap
            decode: nil,                //decode
            shouldInterpolate: false,   //pixel interpolation
            intent: CGColorRenderingIntent.defaultIntent);  //rendering intent
        
        return (UIImage(cgImage: image!), image)
    }
    
    
    // Convert raw depth frame in a renderer image
    func imageFromPixelsGrayscale(_ pixels : UnsafeMutablePointer<UInt8>, width: Int, height: Int) -> UIImage? {
        let colorSpace = CGColorSpaceCreateDeviceGray()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.none.rawValue)
        let provider = CGDataProvider(data: Data(bytes: UnsafePointer<UInt8>(pixels), count: width*height) as CFData)
        
        let image = CGImage(
            width: width,               //width
            height: height,             //height
            bitsPerComponent: 8,        //bits per component
            bitsPerPixel: 8,            //bits per pixel
            bytesPerRow: width,         //bytes per row
            space: colorSpace,          //Quartz color space
            bitmapInfo: bitmapInfo,     //Bitmap info (alpha channel?, order, etc)
            provider: provider!,        //Source of data for bitmap
            decode: nil,                //decode
            shouldInterpolate: false,   //pixel interpolation
            intent: CGColorRenderingIntent.defaultIntent);  //rendering intent
        
        return UIImage(cgImage: image!)
    }
    
    
    // Median filter
    func median_filter(_ image: UIImage) -> UIImage? {
        var filter_image = CIImage(image: image)
        filter_image = filter_image?.applyingFilter("CIMedianFilter")
        return UIImage(ciImage: filter_image!)
    }
    
    
    // Median filter
    func median_filter_ci(_ filter_image: CIImage) -> CIImage? {
        return filter_image.applyingFilter("CIMedianFilter")
    }
    
    
    // Filtro erode, kernel must be an array that "simulate" a matrix
    func erode_filter(_ image_cg: CGImage, kernel: [Float]) -> CGImage? {
        let kernel_dimension:UInt = UInt(Float(kernel.count).squareRoot())
        
        var backgroundcolor: CGFloat = 0
        
        var format = vImage_CGImageFormat(
            bitsPerComponent: UInt32(image_cg.bitsPerComponent),
            bitsPerPixel: UInt32(image_cg.bitsPerPixel),
            colorSpace: Unmanaged.passRetained(image_cg.colorSpace!),
            bitmapInfo: image_cg.bitmapInfo,
            version: 0,
            decode: nil,
            renderingIntent: image_cg.renderingIntent
        )
        var normal_vi_frame_buffer = vImage_Buffer()
        var normal_vi_filter_buffer = vImage_Buffer()
        
        // creo buffer frame originale
        vImageBuffer_InitWithCGImage(&normal_vi_frame_buffer, &format, &backgroundcolor, image_cg, vImage_Flags(kvImageNoFlags))
        
        // creo buffer frame post filtro dilatate
        vImageBuffer_InitWithCGImage(&normal_vi_filter_buffer, &format, &backgroundcolor, image_cg, vImage_Flags(kvImageNoFlags))
        
        // Applico filtro Erode
        vImageErode_PlanarF(&normal_vi_frame_buffer, &normal_vi_filter_buffer, kernel_dimension, kernel_dimension, kernel, kernel_dimension, kernel_dimension, vImage_Flags(kvImageLeaveAlphaUnchanged))
        
        // display image
        let vertical_normal_cg_image_unmanaged = vImageCreateCGImageFromBuffer(&normal_vi_filter_buffer, &format, nil, nil, vImage_Flags(kvImageNoFlags), nil)
        let vertical_normal_cg_image = vertical_normal_cg_image_unmanaged?.takeUnretainedValue()
        
        free(normal_vi_frame_buffer.data)
        free(normal_vi_filter_buffer.data)
        vertical_normal_cg_image_unmanaged?.release()

        return vertical_normal_cg_image
    }
    
    
    //
    // Filtro erode, kernel must be an array that "simulate" a matrix
    func dilate_filter(_ image_cg: CGImage, kernel: [Float]) -> CGImage? {
        let kernel_dimension:UInt = UInt(Float(kernel.count).squareRoot())
        
        var backgroundcolor: CGFloat = 0
        
        var format = vImage_CGImageFormat(
            bitsPerComponent: UInt32(image_cg.bitsPerComponent),
            bitsPerPixel: UInt32(image_cg.bitsPerPixel),
            colorSpace: Unmanaged.passRetained(image_cg.colorSpace!),
            bitmapInfo: image_cg.bitmapInfo,
            version: 0,
            decode: nil,
            renderingIntent: image_cg.renderingIntent
        )
        var normal_vi_frame_buffer = vImage_Buffer()
        var normal_vi_filter_buffer = vImage_Buffer()
        
        // creo buffer frame originale
        vImageBuffer_InitWithCGImage(&normal_vi_frame_buffer, &format, &backgroundcolor, image_cg, vImage_Flags(kvImageNoFlags))
        
        // creo buffer frame post filtro dilatate
        vImageBuffer_InitWithCGImage(&normal_vi_filter_buffer, &format, &backgroundcolor, image_cg, vImage_Flags(kvImageNoFlags))
        
        // Applico filtro Erode
        vImageDilate_PlanarF(&normal_vi_frame_buffer, &normal_vi_filter_buffer, kernel_dimension, kernel_dimension, kernel, kernel_dimension, kernel_dimension, vImage_Flags(kvImageLeaveAlphaUnchanged))
        
        // display image
        let vertical_normal_cg_image_unmanaged = vImageCreateCGImageFromBuffer(&normal_vi_filter_buffer, &format, nil, nil, vImage_Flags(kvImageNoFlags), nil)
        let vertical_normal_cg_image = vertical_normal_cg_image_unmanaged?.takeUnretainedValue()
        
        free(normal_vi_frame_buffer.data)
        free(normal_vi_filter_buffer.data)
        vertical_normal_cg_image_unmanaged?.release()
        
        return vertical_normal_cg_image
    }
    
    
    // Get a false color composition of the normal frame, XYZ -> RGB
    func normal_false_color(_ normal_float: STNormalFrame) -> (uiimage: UIImage?, cgimage: CGImage?) {
        let frame_pixel_number: Int = Int(normal_float.width * normal_float.height)
        
        let normal = normal_float.normals
        // empy frame buffer
        let normal_buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: Int(frame_pixel_number*4))
        
        for j in 0..<frame_pixel_number {
            if(!normal![j].x.isNaN || !normal![j].y.isNaN || !normal![j].z.isNaN) {
                // Convert normal unit vectors (ranging from -1 to 1) to RGB (ranging from 0 to 255)
                // Z can be slightly positive in some cases too!
                
                normal_buffer[4*j+0] = (UInt8)(((normal![j].x / 2 ) + 0.5 ) * 255)
                normal_buffer[4*j+1] = (UInt8)(((normal![j].y / 2 ) + 0.5 ) * 255)
                normal_buffer[4*j+2] = (UInt8)(((normal![j].z / 2 ) + 0.5 ) * 255)
            } else {
                normal_buffer[4*j+0] = 0
                normal_buffer[4*j+1] = 0
                normal_buffer[4*j+2] = 0
            }
        }
        
        let normal_frame = imageFromPixels(normal_buffer, width: Int(normal_float.width), height: Int(normal_float.height))
        
        // deallocate normal frame buffer
        normal_buffer.deallocate()
        
        return (normal_frame.0, normal_frame.1)
    }
    
    /*
    // Segment the pavement from raw normal frame into RGB frame
    func horrizontal_plane_RGB_normal(_ normal_float: STNormalFrame) -> (uiimage: UIImage?, cgimage: CGImage?) {
        let saved_parameter = UserDefaults.standard
        // Load value from disk
        let red_max = saved_parameter.integer(forKey: "red_max")
        let red_min = saved_parameter.integer(forKey: "red_min")
        let green_max = saved_parameter.integer(forKey: "green_max")
        let green_min = saved_parameter.integer(forKey: "green_min")
        let blue_max = saved_parameter.integer(forKey: "blue_max")
        let blue_min = saved_parameter.integer(forKey: "blue_min")
        
        let frame_pixel_number: Int = Int(normal_float.width * normal_float.height)
        // empy frame buffer
        let normal_buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: frame_pixel_number*4)
        let normal = normal_float.normals
        for j in 0..<frame_pixel_number {
            if(!normal![j].x.isNaN || !normal![j].y.isNaN || !normal![j].z.isNaN) {
                // Convert normal unit vectors (ranging from -1 to 1) to RGB (ranging from 0 to 255)
                // Z can be slightly positive in some cases too!
                let red: UInt8 = (UInt8)(((normal![j].x / 2 ) + 0.5 ) * 255)
                let green: UInt8 = (UInt8)(((normal![j].y / 2 ) + 0.5 ) * 255)
                let blue: UInt8 = (UInt8)(((normal![j].z / 2 ) + 0.5 ) * 255)
                
                normal_buffer[4*j+0] = ((red > red_min) && (red < red_max)) ? 0 : 0
                normal_buffer[4*j+1] = ((green >= green_min) && (green < green_max)) ? 255 : 0
                normal_buffer[4*j+2] = ((blue > blue_min) && (blue < blue_max)) ? 0 : 0
            } else {
                normal_buffer[4*j+0] = 0
                normal_buffer[4*j+1] = 0
                normal_buffer[4*j+2] = 0
            }
        }
        
        let normal_frame = imageFromPixels(normal_buffer, width: Int(normal_float.width), height: Int(normal_float.height))
        // deallocate normal frame buffer
        normal_buffer.deallocate()
        
        return (normal_frame.0, normal_frame.1)
    }
    */
    
    // Segment pavement from raw float normal frame into RGBA image
    func horrizontal_plane_float_normal_RGBA(_ normal_float: STNormalFrame) -> (uiimage: UIImage?, cgimage: CGImage?) {
        
        let saved_parameter = UserDefaults.standard
        // Load value from disk
        let green_max = saved_parameter.integer(forKey: "green_max")
        let green_max_float: Float = (Float(green_max)/255 - 0.5) * 2
        
        let green_min = saved_parameter.integer(forKey: "green_min")
        let green_min_float: Float = (Float(green_min)/255 - 0.5) * 2
        
        let frame_pixel_number: Int = Int(normal_float.width * normal_float.height)
        // empy frame buffer
        let normal_buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: frame_pixel_number*4)
        let normal = normal_float.normals
        for j in 0..<frame_pixel_number {
            if(!normal![j].y.isNaN) {
                // Convert normal unit vectors (ranging from -1 to 1) to Grayscale (ranging from 0 to 255)
                // Z can be slightly positive in some cases too!
                // let min_float: Float = -1
                // let max_float: Float = -0.68627
                if ((normal![j].y >= green_min_float) && (normal![j].y <= green_max_float)) {
                    normal_buffer[4*j+0] = 255
                    normal_buffer[4*j+1] = 255
                    normal_buffer[4*j+2] = 255
                }
            } else {
                normal_buffer[4*j+0] = 0
                normal_buffer[4*j+1] = 0
                normal_buffer[4*j+2] = 0
            }
        }
        
        let normal_frame = imageFromPixels(normal_buffer, width: Int(normal_float.width), height: Int(normal_float.height))
        // deallocate normal frame buffer
        normal_buffer.deallocate()
        
        return (normal_frame.uiimage, normal_frame.cgimage)
    }
    
    
    // Segment pavement from raw float normal frame into BW image
    func horrizontal_plane_float_normal(_ normal_float: STNormalFrame) -> UIImage? {
        let frame_pixel_number: Int = Int(normal_float.width * normal_float.height)
        
        // empy frame buffer
        let normal_buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: Int(frame_pixel_number))
            let normal = normal_float.normals
            for j in 0..<frame_pixel_number {
                if(!normal![j].y.isNaN) {
                    // Convert normal unit vectors (ranging from -1 to 1) to Grayscale (ranging from 0 to 255)
                    // Z can be slightly positive in some cases too!
                    let min_float: Float = -1
                    let max_float: Float = -0.68627
                    if ((normal![j].y >= min_float) && (normal![j].y <= max_float)) {
                        normal_buffer[j] = 255
                    }
                } else {
                    normal_buffer[j] = 0
                }
            }
        
        let normal_ui_frame = imageFromPixelsGrayscale(normal_buffer, width: Int(normal_float.width), height: Int(normal_float.height))
        
        // deallocate normal frame buffer
        normal_buffer.deallocate()
        return normal_ui_frame
    }

    
    // Pretty self explanatory - found online
    func convertCIImageToCGImage(inputImage: CIImage) -> CGImage! {
        let context = CIContext(options: nil)
        return context.createCGImage(inputImage, from: inputImage.extent)
    }
    

}
